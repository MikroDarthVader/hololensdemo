﻿using UnityEngine;
using System;

public class UIAnimations : MonoBehaviour
{
    [Serializable]
    public class Animations
    {
        [SerializeField] public bool MovementEnabled = false;
        [SerializeField] public float MovementSpeed = 50.0f;
        [SerializeField] public Ease MovementEase = Ease.Linear;
        [SerializeField] public Vector2 EndPosAnchor;
        [SerializeField] public Vector2 EndPosition;

        [SerializeField] public bool ScalingEnabled = false;
        [SerializeField] public float ScalingSpeed;
        [SerializeField] public Ease ScalingEase = Ease.Linear;
        [SerializeField] public Vector2 EndScale;
    }

    [SerializeField] private RectTransform canvas;
    [SerializeField] private Animations[] animations;
    private RectTransform rect;

    private Vector2 StartPos;
    private Vector2 StartScale;

    private void Start()
    {
        rect = GetComponent<RectTransform>();
        StartPos = rect.anchoredPosition;
        StartScale = rect.localScale;
    }

    public void Reset()
    {
        rect.anchoredPosition = StartPos;
        rect.localScale = StartScale;
    }

    public void Run(int AnimationsInd, Action onComplete = null)
    {
        rect.gameObject.CancelAllTweens();

        if (!animations[AnimationsInd].MovementEnabled && !animations[AnimationsInd].ScalingEnabled)
        {
            onComplete?.Invoke();
            return;
        }

        int endCounter = (animations[AnimationsInd].MovementEnabled ? 1 : 0) + (animations[AnimationsInd].ScalingEnabled ? 1 : 0);

        if (animations[AnimationsInd].MovementEnabled)
            rect.MoveUIAtSpeed(animations[AnimationsInd].EndPosAnchor + animations[AnimationsInd].EndPosition / canvas.rect.size, canvas, animations[AnimationsInd].MovementSpeed).
                SetEase(animations[AnimationsInd].MovementEase).
                SetOnComplete(delegate { endCounter--; if (endCounter == 0) onComplete?.Invoke(); }).
                SetOwner(gameObject);

        if (animations[AnimationsInd].ScalingEnabled)
            rect.ScaleAtSpeed(animations[AnimationsInd].EndScale, animations[AnimationsInd].ScalingSpeed).
                SetEase(animations[AnimationsInd].ScalingEase).
                SetOnComplete(delegate { endCounter--; if (endCounter == 0) onComplete?.Invoke(); }).
                SetOwner(gameObject);
    }
}