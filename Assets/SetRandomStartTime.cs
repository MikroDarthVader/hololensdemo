﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetRandomStartTime : MonoBehaviour
{
    void Start()
    {
        Material mat = new Material(GetComponent<Image>().material);
        mat.SetFloat("_TimeStart", Random.Range(0f, 10000f));
        GetComponent<Image>().material = mat;
    }
}
