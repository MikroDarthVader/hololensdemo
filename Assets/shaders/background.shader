﻿Shader "Unlit/background"
{
	Properties
	{
			_MainTex("Texture", 2D) = "white" {}
			_Intens("Intens", Float) = 1.
			_PowR("Red Intensity", Float) = 1.
			_PowG("Green Intensity", Float) = .95
			_PowB("Blue Intensity", Float) = .5
			_Scale("Scale", Float) = 1.2
			_FlickerRate("FlickerRate", Float) = 4.
	}
		SubShader
			{
				Tags { "RenderType" = "Opaque" }
				LOD 100

				Pass
				{
					CGPROGRAM
					#pragma vertex vert
					#pragma fragment frag

					#include "UnityCG.cginc"

					struct appdata
					{
						float4 vertex : POSITION;
						float2 uv : TEXCOORD0;

						UNITY_VERTEX_INPUT_INSTANCE_ID
					};

					struct v2f
					{
						float2 uv : TEXCOORD0;
						float4 vertex : SV_POSITION;

						UNITY_VERTEX_OUTPUT_STEREO
					};

					sampler2D _MainTex;
					float4 _MainTex_ST;


					float _Intens;
					float _Scale;
					float _PowR;
					float _PowG;
					float _PowB;
					float _FlickerRate;
					float time;


		#define S(a, b, t) smoothstep(a, b, t)
		#define NUM_LAYERS 4.

					float N21(float2 p) {
						float3 a = frac(float3(p.xyx) * float3(213.897, 653.453, 253.098));
						a += dot(a, a.yzx + 79.76);
						return frac((a.x + a.y) * a.z);
					}

					float2 GetPos(float2 id, float2 offs, float t) {
						float n = N21(id + offs);
						float n1 = frac(n * 10.);
						float n2 = frac(n * 100.);
						float a = t + n;
						return offs + float2(sin(a * n1), cos(a * n2)) * .4;
					}

					float GetT(float2 ro, float2 rd, float2 p) {
						return dot(p - ro, rd);
					}

					float LineDist(float3 a, float3 b, float3 p) {
						return length(cross(b - a, p - a)) / length(p - a);
					}

					float df_line(in float2 a, in float2 b, in float2 p)
					{
						float2 pa = p - a, ba = b - a;
						float h = clamp(dot(pa, ba) / dot(ba, ba), 0., 1.);
						return length(pa - ba * h);
					}

					float Line(float2 a, float2 b, float2 uv) {
						float r1 = .04;
						float r2 = .01;

						float d = df_line(a, b, uv);
						float d2 = length(a - b);
						float fade = S(1.5, .5, d2);

						fade += S(.05, .02, abs(d2 - .75));
						return S(r1, r2, d) * fade;
					}

					float NetLayer(float2 st, float n, float t) {
						float2 id = floor(st) + n;

						st = frac(st) - .5;

						float2 p[9];
						int i = 0;
						for (float y = -1.; y <= 1.; y++) {
							for (float x = -1.; x <= 1.; x++) {
								p[i++] = GetPos(id, float2(x, y), t);
							}
						}

						float m = 0.;
						float sparkle = 0.;

						for (int i = 0; i < 9; i++) {
							m += Line(p[4], p[i], st);
							float d = length(st - p[i]);
							float s = (.005 / (d * d));
							s *= S(1., .7, d);
							s *= sin((frac(p[i].x) + frac(p[i].y) + t) * _FlickerRate) * .4 + .6;
							sparkle += s;
						}

						m += Line(p[1], p[3], st) +
							Line(p[1], p[5], st) +
							Line(p[7], p[5], st) +
							Line(p[7], p[3], st);

						return m + sparkle;
					}

					v2f vert(appdata v)
					{
						v2f o;

						UNITY_SETUP_INSTANCE_ID(v);
						UNITY_INITIALIZE_OUTPUT(v2f, o);
						UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

						o.vertex = UnityObjectToClipPos(v.vertex);
						o.uv = TRANSFORM_TEX(v.uv, _MainTex);
						return o;
					}

					fixed4 frag(v2f data) : SV_Target
					{
						//float2 uv = (fragCoord - iResolution.xy * .5) / iResolution.y;
						time = _Time.y + 123.456;
						float t = time * 0.01;

						float s = sin(t);
						float c = cos(t);

						float m = 0.;
						for (float i = 0.; i < 1.; i += 1. / NUM_LAYERS)
						{
							float z = frac(t + i);
							float fade = S(0., .6, z) * S(1., .8, z);
							m += fade * NetLayer(data.uv, i, time);
						}

						data.uv *= _Scale;
						float nl = NetLayer(data.uv, 0., time) * _Intens;
						float r = pow(nl, 1.),
							  g = pow(nl,  .95),
							  b = pow(nl,  .5);
						return float4(r,g,b,1);
					}
					ENDCG
				}
			}
}
