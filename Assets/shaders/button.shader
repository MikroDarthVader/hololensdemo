﻿Shader "Unlit/button"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_MainColor("Color", Color) = (1.,1.,1.,1.)
		_AnimationRate("AnimationRate", Float) = 1.
		_Speed("AnimationSpeed", Float) = 1.
		_TimeStart("TimeStart", Float) = 0.
	}
		SubShader
		{
			Tags { "RenderType" = "Transparent" "IgnoreProjector" = "True"}
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
			LOD 100

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;

					UNITY_VERTEX_INPUT_INSTANCE_ID
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;

					UNITY_VERTEX_OUTPUT_STEREO
				};

				sampler2D _MainTex;
				float4 _MainTex_ST;

				float4 _MainColor;

				float _AnimationRate;
				float _Speed;
				float _TimeStart;

				v2f vert(appdata v)
				{
					v2f o;

					UNITY_SETUP_INSTANCE_ID(v);
					UNITY_INITIALIZE_OUTPUT(v2f, o);
					UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					return o;
				}

				float mod(float x, float y)
				{
					return x - y * floor(x / y);
				}

				bool outer(float time, float2 uv, float far, float dist)
				{
					far += cos(uv.y - time * 8.0) / 50.0;
					if (dist < far) {
						return true;
					}
					return false;
				}

				bool belongs(float time, float2 uv, float near, float far, float sections, float dist, float at2)
				{
					float PI = 3.1415926535;
					near += sin(uv.x - time * 8.0) / 50.0;
					far += cos(uv.y - time * 8.0) / 50.0;

					float angle = mod(at2 + time * 2.5 + sin(time * 4.0) * _Speed, PI * 2.0);
					float oddity = mod(angle / (2.0 * PI) * sections * 2.0, 2.0);

					if ((dist > near) && (dist < far) && floor(oddity) == 0.0)
						return true;
					else
						return false;
				}

				fixed4 frag(v2f i) : SV_Target
				{
					float time = (_Time.y + _TimeStart) * _AnimationRate;
					float2 xy = i.uv - float2(0.5, 0.5);
					float dist = length(xy);
					float at2 = atan2(xy.y, xy.x);

						bool circles = belongs(time,       i.uv, 0.15, 0.2, 4., dist, at2) ||
									   belongs(time + 0.5, i.uv, 0.25, 0.3, 3., dist, at2) ||
									   belongs(time + 1.0, i.uv, 0.35, 0.4, 5., dist, at2);

						if (circles)
							return _MainColor;
						else {
							float a = 0.0;
							//if (outer(time + 1.0, i.uv, 0.45, dist)) a += 1.0;
							return float4(0.0, 0.0, 0.0, a);
						}
					}
					ENDCG
				}
		}
}
