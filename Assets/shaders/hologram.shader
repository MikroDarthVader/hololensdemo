﻿Shader "Unlit/hologram"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Color("Color", Color) = (1., 1., 1., 1.)
		_Scale("Scale", Float) = 1.
	}
		SubShader
		{
			Tags { "RenderType" = "Transparent" "IgnoreProjector" = "True"}
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
			LOD 100

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;

					UNITY_VERTEX_INPUT_INSTANCE_ID
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;

					UNITY_VERTEX_OUTPUT_STEREO
				};

				sampler2D _MainTex;
				float4 _MainTex_ST;
				float4 _Color;
				float _Scale;

				float4 Scanline(float4 color, float2 uv)
				{
					float scanline = clamp(0.95 + 0.05 * cos(3.14 * (uv.y + 0.008 * _Time.y) * 240.0 * 1.0), 0.0, 1.0);
					float grille = 0.85 + 0.15 * clamp(1.5 * cos(3.14 * uv.x * 640.0 * 1.0), 0.0, 1.0);
					return color * scanline * grille * 1.2;
				}

				float rand(float2 seed) {
					float dotResult = dot(seed.xy, float2(12.9898, 78.233));
					float s = sin(dotResult) * 43758.5453;
					return frac(s);
				}

				float4 edgeSample(float2 uv)
				{
					if (uv.x > 1.0) return float4(0., 0., 0., 0.);
					if (uv.x < 0.0) return float4(0., 0., 0., 0.);
					if (uv.y > 1.0) return float4(0., 0., 0., 0.);
					if (uv.y < 0.0) return float4(0., 0., 0., 0.);
					return tex2D(_MainTex, clamp(clamp(uv, 0.0, 1.0), 0.0, 1.0));
				}

				v2f vert(appdata v)
				{
					v2f o;

					UNITY_SETUP_INSTANCE_ID(v);
					UNITY_INITIALIZE_OUTPUT(v2f, o);
					UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					return o;
				}

				fixed4 frag(v2f i) : SV_Target
				{
					float2 uvFlicker = i.uv;

					//flicker
					float f = 0.003;
					uvFlicker.x += rand(float2(0, i.uv.y) * (_Time.y)) * f * _Scale;
					uvFlicker.y += rand(float2(0, i.uv.x) * (_Time.y)) * f * _Scale;

					float inc = 0.2;

					float4 c = Scanline(edgeSample(uvFlicker) * 1.5, i.uv);
					return c * _Color;
				}
				ENDCG
			}
		}
}
