﻿using System.Collections;
using UnityEngine;
using UnityEngine.Video;

public class UIManager : MonoBehaviour
{
    public UIAnimations Button1Movement;
    public UIAnimations Button2Movement;
    public UIAnimations Button3Movement;
    public UIAnimations Video1;
    public UIAnimations Video2;
    public UIAnimations Video3;
    public VideoPlayer VideoPlayer1;
    public VideoPlayer VideoPlayer2;
    public VideoPlayer VideoPlayer3;
    public UIAnimations InfoText1;
    public UIAnimations InfoText2;
    public UIAnimations InfoText3;

    public UIAnimations buttonCloseVideo;

    private UIAnimations[] buttons;
    private UIAnimations[] videos;
    private VideoPlayer[] videoPlayers;
    private UIAnimations[] infoTexts;

    private int OpenedVideo;

    private int ActiveAnims;

    private void Start()
    {
        buttons = new UIAnimations[] { Button1Movement, Button2Movement, Button3Movement };
        videos = new UIAnimations[] { Video1, Video2, Video3 };
        videoPlayers = new VideoPlayer[] { VideoPlayer1, VideoPlayer2, VideoPlayer3 };
        infoTexts = new UIAnimations[] { InfoText1, InfoText2, InfoText3 };

        OpenedVideo = -1;
        ActiveAnims = 0;

        for (int i = 0; i < videoPlayers.Length; i++)
        {
            float aspectRatio = (float)videoPlayers[i].height / videoPlayers[i].width;
            var rectTransform = videoPlayers[i].GetComponent<RectTransform>();
            rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, rectTransform.rect.width * aspectRatio - rectTransform.rect.height);

            videoPlayers[i].time = 0;
            videoPlayers[i].Play();
            videoPlayers[i].Pause();
        }
    }

    public void ShowVideo1() { if (ActiveAnims != 0 || OpenedVideo == 0) return; StartCoroutine(ShowVideo(0)); }
    public void ShowVideo2() { if (ActiveAnims != 0 || OpenedVideo == 1) return; StartCoroutine(ShowVideo(1)); }
    public void ShowVideo3() { if (ActiveAnims != 0 || OpenedVideo == 2) return; StartCoroutine(ShowVideo(2)); }
    public void CloseVideo() { if (ActiveAnims != 0 || OpenedVideo == -1) return; StartCoroutine(closeVideo()); }

    public IEnumerator ShowVideo(int ind)
    {
        if (OpenedVideo == -1)
        {
            ActiveAnims += buttons.Length + 3;

            buttons[ind].Run(2, delegate { ActiveAnims--; });
            yield return new WaitForSeconds(0.1f);
            int upInd = ind + 1;
            int downInd = ind - 1;
            while (upInd < buttons.Length || downInd >= 0)
            {
                if (upInd < buttons.Length)
                    buttons[upInd].Run(0, delegate { ActiveAnims--; });
                if (downInd >= 0)
                    buttons[downInd].Run(0, delegate { ActiveAnims--; });
                upInd++;
                downInd--;
                yield return new WaitForSeconds(0.1f);
            }
            yield return new WaitForSeconds(0.6f);

            videos[ind].Run(0, delegate { ActiveAnims--; videoPlayers[ind].Play(); });
            yield return new WaitForSeconds(0.1f);
            infoTexts[ind].Run(0, delegate { ActiveAnims--; });
            yield return new WaitForSeconds(0.9f);
            buttonCloseVideo.Run(0, delegate { ActiveAnims--; });
        }
        else
        {
            ActiveAnims += 6;

            // close opened video
            var video = videos[OpenedVideo];
            var infoText = infoTexts[OpenedVideo];
            var videoPlayer = videoPlayers[OpenedVideo];

            videoPlayer.Stop();
            buttons[OpenedVideo].Run(5, delegate { ActiveAnims--; });
            buttons[ind].Run(4, delegate { ActiveAnims--; });
            video.Run(1, delegate { video.Reset(); ActiveAnims--; videoPlayer.time = 0; videoPlayer.Play(); videoPlayer.Pause(); });
            yield return new WaitForSeconds(0.1f);
            infoText.Run(1, delegate { infoText.Reset(); ActiveAnims--; });
            yield return new WaitForSeconds(1.1f);

            // open new video
            videos[ind].Run(0, delegate { ActiveAnims--; videoPlayers[ind].Play(); });
            yield return new WaitForSeconds(0.1f);
            infoTexts[ind].Run(0, delegate { ActiveAnims--; });
        }

        OpenedVideo = ind;
    }

    public IEnumerator closeVideo()
    {
        ActiveAnims += buttons.Length + 3;

        var video = videos[OpenedVideo];
        var infoText = infoTexts[OpenedVideo];
        var videoPlayer = videoPlayers[OpenedVideo];

        videoPlayer.Stop();
        buttonCloseVideo.Run(1, delegate { ActiveAnims--; });
        video.Run(1, delegate { video.Reset(); ActiveAnims--; videoPlayer.time = 0; videoPlayer.Play(); videoPlayer.Pause(); });
        yield return new WaitForSeconds(0.1f);
        infoText.Run(1, delegate { infoText.Reset(); ActiveAnims--; });
        yield return new WaitForSeconds(1f);
        for (int i = buttons.Length - 1; i >= 0; i--)
        {
            if (i == OpenedVideo)
                buttons[i].Run(3, delegate { ActiveAnims--; });
            else
                buttons[i].Run(1, delegate { ActiveAnims--; });
            yield return new WaitForSeconds(0.1f);
        }

        OpenedVideo = -1;
    }
}
